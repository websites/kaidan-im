---
SPDX-FileCopyrightText: 20YY Alice A <alice@example.org>
SPDX-FileCopyrightText: 20YY Bob B <bob@example.com>
SPDX-License-Identifier: CC0-1.0
layout: post
title: "Template for Posts"
date: 20YY-MM-DD 20:00:00 +01:00
author: alice, bob
---

## This is a Big Heading

This is the first paragraph belonging to the first heading.

### This is a Smaller Heading

This is the first paragraph belonging to the second heading.
This belongs to the same paragraph as the line before.

This is the second paragraph belonging to the second heading.

If you want to embed images, you can do that with the following exemplary line:
![image description]({{ "/images/screenshots/2020-01-10-quick-onboarding.png" | prepend: site.baseurl }})

You can embed an image having a specific styling:
<img alt="OMEMO logo"
     src="{{ "/images/omemo.png" | prepend: site.baseurl }}"
     style="height: 8em; margin: 0.5em; float: left"/>

If you want to add an internal link, you can do that with the following exemplary line:
[link text]({{ "/2020/04/06/kaidan-0.5.0/" | prepend: site.baseurl }})

An external link can be added with the following exemplary line:
[link text](https://example.org/example/)
