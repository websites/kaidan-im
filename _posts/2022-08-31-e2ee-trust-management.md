---
SPDX-FileCopyrightText: 2022 Melvin Keskin <melvo@olomono.de>
SPDX-License-Identifier: CC0-1.0
layout: post
title: "Kaidan's End-to-End Encryption Trust Management"
date: 2022-08-31 12:00:00 +02:00
author: melvo
---

We worked several months on Kaidan's upcoming end-to-end encryption and trust management.
Once Kaidan 0.9 is released, it will provide the latest [OMEMO Encryption](/2021/01/07/end-to-end-encryption/).
But it will also make trust decisions in the background for you if it's possible.
Some trust decisions have to be made manually but there are many others Kaidan automates without decreasing your security.
That is done by automatically sharing trust decisions via already secured channels.

The feature Kaidan uses is called [Automatic Trust Management (ATM)](https://github.com/olomono/atm).
Your device receives the encryption data to secure the conversation between you and your contact via the internet.
That encryption data can be the data of an attacker.
While you think that you communicate with your contact securely, the attacker can read, modify or drop everything you exchange.

You have to make sure that the encryption data you received are really those from your contact to detect and stop an attack.
That is done by comparing the exchanged encryption data via a second secure channel.
Kaidan provides QR codes for that.
QR codes are especially helpful when you want a secure conversation with a contact you can meet in person.
For that, you simply scan your contact's QR code and vice versa.

<img alt="First QR code scan"
     src="{{ "/images/qr-code-scan-1.svg" | prepend: site.baseurl }}"
     style="height: 14em; margin: 0.5em; float: left"/>

<img alt="Second QR code scan"
     src="{{ "/images/qr-code-scan-2.svg" | prepend: site.baseurl }}"
     style="height: 14em; margin: 0.5em; float: left"/>

But what if your contact used a smartphone during the first QR code scan and now wants to chat with you via a notebook too?
Usually, *your contact* would have to scan the notebook's QR code with the smartphone and vice versa.
Furthermore, *you* would have to scan the notebook's QR code and vice versa.
If you or your contact gets another device or even replaces an old one, QR codes have to be scanned again.
That leads to multiple mutual scans, one for each pair of devices.

In the following example graph, there are four devices.
B1, B2 and B3 could be your contact's devices and A1 yours.
The six gray edges are the mutual QR code scans that are needed to stop all possible attacks.

<img alt="Needed trust decisions"
     src="{{ "/images/e2ee-trust-graph.svg" | prepend: site.baseurl }}"
     style="height: 14em; margin: 0.5em; float: right"/>

With ATM, many QR code scans between you and your contact become unnecessary.
The first meeting is used for the initial scan.
The encryption data of all new devices is checked via the secure channel established by it.
If your contact wants to chat via the notebook, your contact simply scans the notebook via the smartphone and vice versa.
But all other scans are not needed anymore.
The trust decisions are communicated between the devices that already have a secure channel.

In the example graph, ATM could work as follows:
1. A1 scans B1's QR code and vice versa.
1. A1 scans A2's QR code and vice versa.
1. A2 scans A3's QR code and vice versa.
1. The remaining three edges are created automatically by ATM via the existing secure channels.

If you want to try out that new feature, stay tuned!
Our next release will come soon.
