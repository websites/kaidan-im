---
layout: post
title: "Kaidan for the Masses: Our Upcoming 9-seconds-registration"
date: 2020-01-08 19:00:00 +01:00
author: zatrox, jbb, melvo
---

### What's XMPP's biggest problem? - Accessibility!

Sure, it is accessible for the people who are really interested in XMPP or want to be more secure, but the normal user doesn't want to study XMPP before they know what to do.

That's why we work on an easy-to-use registration, which makes all decisions for a new user, but still ensures the highest possible security and decentralization. This means that even the password is randomly generated (it is changeable later on). In the end it only takes a few clicks to get to your new account, which is hosted by an automatically chosen public server which supports all of Kaidan's features.

The user may choose to use the suggested server, username and password or to use own values. So, now switching from your old messenger to Kaidan (or other XMPP-based clients) is much easier. Therefore, you can invite your friends to XMPP and instantly start chatting with them.

### Our aim is to make XMPP more accessible for everyone.
### This is our first step towards that goal.

## So, here is a quick look at the new registration that will come in Kaidan 0.5.0:
At first, you're asked whether you already own an account or want to create a new one. If you have an own account, you can log in manually or by scanning a QR code which contains your credentials.
After that, the user is asked whether the (automatic) "9-seconds-registration" should be used or the manual registration. 

![screenshot of registration]({{ "/images/screenshots/2020-01-08-register-path.png" | prepend: site.baseurl }})

If you choose the automatic registration, in most cases you only need to solve a CAPTCHA and you're done.

If you choose the manual registration, you can enter a display name and choose a server which is also randomly pre-selected. If it is possible, the server is selected depending on your system's country settings. In the next step, Kaidan generates a username for you by alternating vowels and consonants but you are free to choose an own one.

![screenshot of registration server selection]({{ "/images/screenshots/2020-01-08-choose-server.png" | prepend: site.baseurl }})

The next step is choosing a password. If you don't enter a password yourself, Kaidan will generate a secure one for you. It is changeable at any time.
Depending on the server, the last step might be to enter further information or to solve a CAPTCHA.
And then you can start chatting!
If you only want to chat like with the dominating chat services and don't care about the server, username or password, your choice is the automatic registration. It takes only nine seconds to start chatting.
If you want to choose an own server, username or password like you can do with most XMPP clients, the manual registration is for you.
No matter which way you choose, you will be ready for chatting without any hassle.
### The registration is fast and simple, perfect for new users!

![screenshot of manual registration]({{ "/images/screenshots/2020-01-08-register-manually.png" | prepend: site.baseurl }})

If an account is not used anymore, it can be deleted from the client and server too.

![screenshot of account deletion]({{ "/images/screenshots/2020-01-08-delete-account.png" | prepend: site.baseurl }})

The presented features are not part of Kaidan yet, but will come with the new Kaidan 0.5.0.

All choices are depicted by cool cliparts, making it clearer what happens when you press any button.
Special thanks to MBB, who made those cliparts.
