---
SPDX-FileCopyrightText: 2024 Melvin Keskin <melvo@olomono.de>
SPDX-License-Identifier: CC0-1.0
layout: post
title: "Kaidan 0.9.2: Media Sharing Fixes"
date: 2024-07-24 00:00:00 00:00
author: melvo
---

This release fixes some bugs.
Have a look at the [changelog](#changelog) for more details.

## Changelog

Bugfixes:

* Fix file extension for downloads when mime type is empty (lnj)
* Fix file downloads without a source URL could be started (lnj)
* Fix file messages are never marked as sent (lnj)
* Fix message body of previous file selection was used (lnj)
* Fix missing receipt request (for green checkmark) on media messages (lnj)
* Fix outgoing encrypted media messages are displayed as unencrypted (lnj)

## Download

{% include downloads_0.9.2.md %}
