---
layout: post
title: "Kaidan will receive a grant for end-to-end encryption"
date: 2021-01-07 14:00:00 +01:00
author: melvo
---

<img alt="OMEMO logo"
     src="{{ "/images/omemo.png" | prepend: site.baseurl }}"
     style="height: 8em; margin: 0.5em; float: left"/>

[Kaidan](https://nlnet.nl/project/Kaidan/) will be supported by [NLnet](https://nlnet.nl/foundation/) for adding end-to-end encryption support.
We will implement the latest version of the encryption protocol [OMEMO](https://xmpp.org/extensions/xep-0384.html) and extend it by an easy trust management to solve issues other OMEMO-capable chat apps suffer from.

## Fundamentals

End-to-end encryption ensures that sent data can only be read by the intended recipients.
Signing is used to ensure that received data comes from the intended sender and was not modified in transit.
To read encrypted data, it has to be decrypted before.

Keys are used to encrypt and decrypt data.
Those keys are like very strong passwords.
If the data is encrypted and decrypted with the same key, the procedure is called symmetric encryption.

**OMEMO** enables the users to chat end-to-end encrypted.
It is an asymmetric encryption protocol.
That means, different keys are used to encrypt and decrypt the data.
Each communication partner has a private and a public key.
A private key is used to sign and decrypt data.
A public key is used to encrypt data and verify its signature.

## Metadata

The latest version of OMEMO uses [Stanza Content Encryption](https://xmpp.org/extensions/xep-0420.html).
That means, in addition to messages and files such as images, it is now possible to end-to-end encrypt metadata as well.
Such metadata could be e.g. the indicator whether someone is currently composing a message.
Kaidan will use [Chat State Notifications](https://xmpp.org/extensions/xep-0085.html) for that purpose and encrypt them with OMEMO.
Furthermore, many features already supported, like delivery receipts and message correction, are going to be automatically end-to-end encrypted.

## Trust Management

When a user starts an encrypted chat, OMEMO retrieves the recipient's public key automatically.
Since there is no secure channel yet, that key might not belong to the intended recipient but to an attacker controlling the infrastructure in between.
*Trust management* is the process of deciding whether a retrieved public key should be trusted and thus used for encryption and signature verification.
[Trust Messages](https://xmpp.org/extensions/xep-0434.html) in combination with [Automatic Trust Management](https://xmpp.org/extensions/xep-0450.html) simplify the trust management.

Nowadays, it is possible to use OMEMO relatively hassle-free with several XMPP clients but the trust management is still complicated.
It becomes even more difficult when someone uses a new device.
With Automatic Trust Management, Kaidan will be the first XMPP chat app introducing an easy trust management.
It automates as much as possible while keeping the same security level as with a completely manual trust management.

## Free and Secure Communication

As far as we know, there are no other implementations of Stanza Content Encryption and Automatic Trust Management in use at the moment.
We will take the first steps and hope that other XMPP developers will follow our lead.
XMPP client developers interested in implementing an easy trust management should have a look at [ATM's project site](https://github.com/olomono/atm).
It contains all relevant links, pseudocode and test cases.

Kaidan's goal is to **minimize the effort for establishing free and secure communication**, thus lowering the entry barrier to strong encryption mechanisms for the average user.
NLnet and [a couple of other organizations](https://nlnet.nl/PET/background/#organisation) support us via the [NGI Zero PET](https://nlnet.nl/PET/) fund to achieve that.
The money is provided by the European Commission.
That is a great example (like the previous support by the [DBJR (German Federal Youth Council)](https://tooldoku.dbjr.de)) of how [public money can be used to develop free software](https://publiccode.eu).

There are [many other interesting projects currently funded by NLnet](https://nlnet.nl/project/current.html).
We are glad that Kaidan is one of them!

<div>
<a href="https://nlnet.nl">
<img alt="NLnet logo"
     src="{{ "/images/nlnet.png" | prepend: site.baseurl }}"
     style="height: 4em; margin: 0.5em"/>
</a>

<a href="https://nlnet.nl/NGI0/">
<img alt="NGI Zero logo"
     src="{{ "/images/ngizero.png" | prepend: site.baseurl }}"
     style="height: 4em; margin: 0.5em"/>
</a>
</div>
