---
layout: post
title: "Kaidan 0.3.0 released - Gloox-rewrite, Avatars & Carbons"
date: 2017-08-15 08:45:00 +02:00
author: lnj
---

## Changelog
 * Added XEP-0280: Message Carbons (#117) (LNJ)
 * Added XEP-0054/XEP-0153: VCard-based avatars (#73, #105, #119) (LNJ)
 * Added file storage for simply caching all avatars (LNJ)
 * New roster design - showing round avatars and last message (#118) (LNJ)
 * New chat page design - showing time, delivery status and round avatars (#123) (LNJ)
 * Switched to XMPP client library "gloox" (#114) (LNJ)
 * Rewritten most of the back-end for gloox and partialy also restructured it (#114) (LNJ)
 * (Re)written new LogHandler for gloox (Swiften had this already included) (#114) (LNJ)

**Download**: [kaidan-v0.3.0.tar.gz](https://invent.kde.org/KDE/kaidan/-/archive/v0.3.0/kaidan-v0.3.0.tar.gz)
