---
SPDX-FileCopyrightText: 2022 Melvin Keskin <melvo@olomono.de>
SPDX-License-Identifier: CC0-1.0
layout: post
title: "Encrypted Audio and Video Calls"
date: 2022-09-03 12:00:00 +02:00
author: melvo
---

<img alt="OMEMO logo"
     src="{{ "/images/omemo.png" | prepend: site.baseurl }}"
     style="height: 8em; margin: 0.5em; float: left"/>

[Kaidan](https://nlnet.nl/project/Kaidan/) will receive a grant by [NLnet](https://nlnet.nl/foundation/) for adding encrypted audio and video calls.

The calls will be end-to-end encrypted and authenticated via [OMEMO]({{ "/2021/01/07/end-to-end-encryption/" | prepend: site.baseurl }}).
Furthermore, Kaidan will support small group calls.
We strive for interoperability between Kaidan and other XMPP apps supporting calls.
In order to achieve that, we will implement a large number of XEPs (XMPP Extension Protocols).
They make it possible to have a modern call experience.

Calls have become more widespread over the past few years in free XMPP chat apps.
Especially, grants by NLnet made that possible.
The development speed and interoperability between different apps have increased.
Such an intense development often results in improved specifications.
But sometimes the development overtakes the standardization.
In that case, the standardization needs to catch up what has already been implemented.

We have to handle that circumstance with group calls and invitations to calls at the moment.
There are some adjustments that are not yet official.
To make calls from Kaidan to other apps already supporting them, we have to base our work on those changes.
If those unofficial adjustments are being modified later to become official, we will need to modify Kaidan as well.
But we see the evolution of calls in XMPP as a huge progress and are ready for adaption!

Kaidan's goal is to **establish free and secure communication while being easy to use**, thus lowering the entry barrier to XMPP for average users coming from other networks.
NLnet and [a couple of other organizations](https://nlnet.nl/PET/background/#organisation) support us via the [NGI Zero PET](https://nlnet.nl/PET/) fund to achieve that.
The money is provided by the European Commission.
That is an example of how software can be developed according to the [Public Money, Public Code initiative](https://publiccode.eu).

There are [many other interesting projects currently funded by NLnet](https://nlnet.nl/project/current.html).
We are glad that Kaidan is one of them!

<div>
<a href="https://nlnet.nl">
<img alt="NLnet logo"
     src="{{ "/images/nlnet.png" | prepend: site.baseurl }}"
     style="height: 4em; margin: 0.5em"/>
</a>

<a href="https://nlnet.nl/NGI0/">
<img alt="NGI Zero logo"
     src="{{ "/images/ngizero.png" | prepend: site.baseurl }}"
     style="height: 4em; margin: 0.5em"/>
</a>
</div>
