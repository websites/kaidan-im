---
layout: page
title: Download
permalink: /download/
sitemap: true
---

### Stable release

Downloads for **Kaidan {{ site.latest_release }}**:

{% assign downloads = "downloads_" | append: site.latest_release | append: ".md" %}
{% include {{ downloads }} %}

### Nightly builds

#### Linux

 * [Nightly Flatpak](https://invent.kde.org/network/kaidan/-/wikis/using/flatpak)

### Source code

Kaidan's source code can be found at [invent.kde.org](https://invent.kde.org/network/kaidan).
