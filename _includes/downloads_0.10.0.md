* [Source code (.tar.xz)](https://download.kde.org/unstable/kaidan/0.10.0/kaidan-0.10.0.tar.xz) ([sig](https://download.kde.org/unstable/kaidan/0.10.0/kaidan-0.10.0.tar.xz.sig) signed with [04EFAD0F7A4D9724](https://keys.openpgp.org/vks/v1/by-fingerprint/AE08C590A7D112C1979D068B04EFAD0F7A4D9724))
* [Linux (Flatpak on Flathub)](https://flathub.org/apps/details/im.kaidan.kaidan)

Or install Kaidan for your distribution:

<a href="https://repology.org/project/kaidan/versions">
    <img src="https://repology.org/badge/vertical-allrepos/kaidan.svg?columns=4" alt="Packaging status">
</a>
