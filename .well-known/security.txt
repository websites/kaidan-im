Contact: mailto:security@kde.org
Expires: 2027-03-11T23:00:00.000Z
Encryption: https://kde.org/info/security/
Acknowledgments: https://kde.org/info/security/
Preferred-Languages: en
Canonical: https://www.kaidan.im/.well-known/security.txt
Policy: https://kde.org/info/security/
