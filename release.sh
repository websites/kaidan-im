#!/bin/bash -e

if [ "${1}" = "--help" ] || [ "${1}" = "" ] || [ "${2}" = "" ]
then
  echo "Prepares Kaidan releases."
  echo "Usage: $(basename ${0}) <version> <date>"
  echo "Example: $(basename ${0}) 0.9.2 2024-07-24"
  exit
fi

release_version="${1}"
release_date="${2}"

release_version_pattern="[0-9]*\.[0-9]*\.[0-9]*"
release_date_default_time="00:00:00 00:00"
file_extension=".md"

config_file_release_version_key="latest_release:"
config_file_release_date_key="latest_release_date:"
config_file_path="_config.yml"

download_include_file_path_prefix="_includes/downloads_"
latest_download_include_file_path=$(ls -v "${download_include_file_path_prefix}"* | tail -n 1)
new_download_include_file_path="${download_include_file_path_prefix}${release_version}${file_extension}"

post_file_release_date_key="date:"
post_file_path_prefix="_posts/"
latest_post_file_path=$(ls -v "${post_file_path_prefix}"* | tail -n 1)
new_post_file_path="${post_file_path_prefix}${release_date}-kaidan-${release_version}${file_extension}"

# Replace the old release version and date with the new ones.
sed -i "/${config_file_release_version_key}/c\\${config_file_release_version_key} ${release_version}" "${config_file_path}"
sed -i "/${config_file_release_date_key}/c\\${config_file_release_date_key} ${release_date} ${release_date_default_time}" "${config_file_path}"

# Create a new download include file based on the latest one.
cp "${latest_download_include_file_path}" "${new_download_include_file_path}"


# Replace all occurrences of the old version in the new download include file with the new one.
sed -i "s/${release_version_pattern}/${release_version}/g" "${new_download_include_file_path}"

# Create a new post file based on the latest one.
cp "${latest_post_file_path}" "${new_post_file_path}"

# Replace the old release date with the new one.
sed -i "/${post_file_release_date_key}/c\\${post_file_release_date_key} ${release_date} ${release_date_default_time}" "${new_post_file_path}"

# Replace all occurrences of the old version in the new post file with the new one.
sed -i "s/${release_version_pattern}/${release_version}/g" "${new_post_file_path}"

echo "1. Add/Remove download links to/from ${new_download_include_file_path}"
echo "2. Replace the content of ${new_post_file_path} for the new release"
echo "3. Create a commit and push it: git checkout -b release/${release_version} && git add . && git commit -m 'Release Kaidan ${release_version}' && git push"
